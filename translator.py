# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
# pylint: disable=too-many-locals
# pylint: disable=too-many-branches
# pylint: disable=too-many-statements
# pylint: disable=invalid-name
# pylint: disable=line-too-long

import json
import re
import sys
from dataclasses import dataclass
from json import dumps
from typing import Optional

from isa import Opcode, args_count, Operand, Instruction, OperandType, write_code
from utils import prepare_code, parse_asm_args

NUMBER = r'-?[0-9]+'
LABEL = r'\.?[A-Za-z_]+'
STRING = r'^(\'.*\')|(\".*\")$'

symbol2opcode = {
    "LD": Opcode.LD,
    "ST": Opcode.ST,
    "ADD": Opcode.ADD,
    "MUL": Opcode.MUL,
    "DIV": Opcode.DIV,
    "JMP": Opcode.JMP,
    "HLT": Opcode.HLT,
    "CMP": Opcode.CMP,
    "JE": Opcode.JE,
    "OUT": Opcode.OUT,
    "IN": Opcode.IN,
    "INTER_START": Opcode.INTER_START,
    "INTER_END": Opcode.INTER_END

}

commands = {
    "LD", "ST", "ADD", "MUL", "DIV", "JMP", "HLT", "CMP", "JE", "OUT", "IN", "INTER_START", "INTER_END"
}


@dataclass
class OperandDto:
    type: str
    value: int

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)


@dataclass
class OpcodeDto:
    opcode: str
    operand: OperandDto

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)


def parse_label(val: str) -> Optional[re.Match]:
    label = re.match(LABEL, val)
    if label:
        return label[0][:-1]
    return None


def parse_operand(operand: str) -> Operand:
    if operand.startswith('#'):
        return Operand(OperandType.CONSTANT, (operand[1:]))
    elif operand.startswith('$'):
        return Operand(OperandType.INDIRECT, (operand[1:]))
    elif operand.startswith('.'):
        return Operand(OperandType.LABEL, operand)
    else:
        return Operand(OperandType.DIRECT, operand)


def parse_instruction(line: str) -> Instruction:
    strs = line.split()
    assert len(strs) > 0, 'Line is empty'
    instr = strs[0].upper()
    assert instr in commands, 'Illegal instruction!'
    instructsion: Opcode = symbol2opcode[instr]
    assert len(strs) - 1 == args_count[instructsion], 'Illegal operands count!'

    operand = None
    if len(strs) == 2:
        operand = parse_operand(strs[1])
    return Instruction(instructsion, operand)


def parse_data_block(text) -> [int]:
    data_arr = []
    data_map = {}
    for line in text.split("\n"):
        label, val = line.split(':', 1)
        splitted_line = line.strip().split(maxsplit=2)
        if splitted_line[1] == 'num':
            assert len(splitted_line) == 3
            data_map[splitted_line[0][:-1]] = len(data_arr)
            data_arr.append(int(splitted_line[2]))
        elif splitted_line[1] == 'str':
            data_map[splitted_line[0][:-1]] = len(data_arr)
            string = str(val[len(' str "'):-1]).replace('\\n', '\n').replace('\\t', '\t').replace('\\r', '\r')
            for char in string:
                data_arr.append(ord(char))
    return data_arr, data_map


def parse_text_block(text):
    instructions = []
    labels_map = {}
    for line in text.split("\n"):
        if ':' in line:
            label, *_ = map(str.strip, line.split(':', 1))
            labels_map[label] = len(instructions)
        else:
            instruction = parse_instruction(line)
            instructions.append(instruction)
    return instructions, labels_map


def translate(text):
    code = []
    start_parse_data_index = text.find('section .data')
    assert start_parse_data_index != -1
    start_parse_text_index = text.find('section .text')
    assert start_parse_text_index != -1
    data_arr, data_map = parse_data_block(
        text[start_parse_data_index + len('section .data') + 1:start_parse_text_index - 1])
    instructions, labels_map = parse_text_block(text[start_parse_text_index + len('section .text') + 1:])
    print(data_map)
    print(labels_map)
    for instr in instructions:
        print(instr)
        opercode = instr.name.value
        operandtype = None
        operandvalue = None

        if instr.operand is not None:
            operandtype = instr.operand.type
            operandvalue = instr.operand.value

            if operandtype == OperandType.LABEL:
                operandvalue = labels_map[operandvalue[1:]]
            elif operandtype == OperandType.INDIRECT:
                operandvalue = data_map[operandvalue]
            elif operandtype == OperandType.DIRECT:
                operandvalue = data_map[operandvalue]
            elif operandtype == OperandType.CONSTANT and not operandvalue.lstrip('-').isdigit():
                operandvalue = data_map[operandvalue]
            else:
                operandvalue = int(operandvalue)
            if operandtype == OperandType.LABEL and (opercode == Opcode.JMP or opercode == Opcode.JE):
                operandtype = OperandType.CONSTANT
            operandtype = operandtype.value

        operandDto = OperandDto(operandtype, operandvalue)
        opcodeDto = OpcodeDto(opercode, operandDto)
        code.append(opcodeDto)
    return code, data_arr


def main(args):
    assert len(args) == 2, \
        "Wrong arguments: translator.py <input_file> <target_file>"
    args = parse_asm_args()
    text = args.input_file.readlines()

    source = prepare_code(text)
    code, data = translate(source)
    print("source LoC:", len(source.split()), "code instr:", len(code))
    args.output_file.write(dumps({
        'data': data,
        'code': code
    }, default=lambda o: o.__dict__,
        sort_keys=True, indent=4))


if __name__ == '__main__':
    main(sys.argv[1:])

# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring

import argparse
import os
import re
from typing import List


def prepare_code(code: List[str]) -> str:
    prepared_code = map(lambda l: re.sub(r';.*', '', l), code)
    prepared_code = map(str.strip, prepared_code)
    prepared_code = filter(bool, prepared_code)
    prepared_code = map(lambda l: re.sub(r'\s+', ' ', l), prepared_code)
    return '\n'.join(prepared_code)


def is_valid_file(parser, arg):
    if not os.path.exists(arg):
        parser.error("The file %s does not exist!" % arg)
    else:
        return open(arg, 'r')


def parse_asm_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=argparse.FileType('r'), help='.asm file')
    parser.add_argument('output_file', type=argparse.FileType('w'), help='.out file')
    return parser.parse_args()


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(prog='asm translator')
    parser.add_argument('machine_file', type=argparse.FileType('r'), help='.json file')
    parser.add_argument('input_file', type=argparse.FileType('r'), help='.txt file')
    return parser.parse_args()


def parse_input_buffer(text) -> map:
    prepared_text = map(lambda l: re.sub('/[^a-zA-Z0-9 ]/g', '', l), text)
    return prepared_text

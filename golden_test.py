# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=line-too-long
import logging
import os.path
import tempfile
from unittest.mock import patch
import golden
import pytest

import machine
import translator


@pytest.mark.golden_test("golden/*.yml")
def test_whole_by_golden(golden, caplog):
    caplog.set_level(logging.DEBUG)

    with tempfile.TemporaryDirectory() as tmpdir:
        source = os.path.join(tmpdir, 'source.asm')
        input_buffer = os.path.join(tmpdir, 'input.txt')
        output = os.path.join(tmpdir, 'output.json')

        with open(source, 'w', encoding='utf-8') as file:
            file.write(golden['source'])
        with open(input_buffer, 'w', encoding='utf-8') as file:
            file.write(golden['input'])

        with patch("sys.argv", ["main", source, output]) as out:
            translator.main([source, output])
        with patch("sys.argv", ["main", output, input_buffer]) as out:
            machine.main2([output, input_buffer])

        with open(output, encoding='utf-8') as file:
            code = file.read()

        assert code == golden.out['code']
        # assert stdout.getvalue() == golden.out['output']
        assert caplog.text == golden.out['log']

import io
import json
import logging
import sys
from ast import literal_eval
from typing import List

from isa import read_code, Opcode, StateType, OperandType, SelectType
from utils import parse_args, parse_input_buffer


class DataPath:
    def __init__(self, data_memory_size, data_input, input_buffer):
        # assert len(input_buffer) < data_memory_size, "Data exceeds the maximum memory size!"
        assert data_memory_size > 0, "Data memory size should be non-zero!"
        self.acc = 0
        self.alu = 0
        self.address_register = 0
        self.data_register = 0
        self.input_buffer = input_buffer
        self.output_buffer = []
        self._zero = True
        self.data = [0] * data_memory_size

        self.min_value = -2 ** 31
        self.max_value = 2 ** 31 - 1

        for i, val in enumerate(data_input):
            self.data[i] = val

    def zero(self):
        return self.acc == 0

    def set_zero(self, val):
        self._zero = val == 0

    def negative(self):
        return self.acc < 0

    def has_next_token(self):
        return len(self.input_buffer)

    def next_token(self):
        return ord(self.input_buffer.pop(0)[1])

    def latch_data_reg(self):
        self.data_register = self.data[self.address_register]

    def latch_memory(self):
        self.data[self.address_register] = self.acc

    def latch_acc(self, sel: SelectType):
        if sel is SelectType.DATA:
            self.acc = self.data[self.address_register]
        elif sel is SelectType.INPUT:
            self.acc = ord(self.input_buffer.pop(0)[1])
        elif sel is SelectType.ALU:
            self.acc = self.alu

    def put_data_reg(self, val: int):
        assert self.min_value <= val <= self.max_value, "Overflow!"
        self.data_register = val

    def put_addr_reg(self, val: int):
        assert self.min_value <= val <= self.max_value, "Overflow!"
        self.address_register = val

    def alu_execute(self, code: Opcode):
        result = 0
        if code == Opcode.ADD:
            result = self.acc + self.data_register
            if result > self.max_value:
                result = self.min_value + (result - self.max_value) - 1
            elif result < self.min_value:
                result = self.max_value - (self.min_value - result) + 1
        if code == Opcode.DIV:
            result = self.acc / self.data_register
        return result

    def write(self, code: Opcode):
        self.acc = self.alu_execute(code)

    def output(self, char):
        if char and (self.acc > 64 or self.acc == 32):
            symbol = chr(self.acc)
        else:
            symbol = self.acc
        logging.info('output: %s << %s', repr(''.join(self.output_buffer)), repr(symbol))
        self.output_buffer.append(str(symbol))


class ControlUnit:
    def __init__(self, code, data_path: DataPath):
        self.code = code
        self._tick = 0
        self.prog_counter = 0
        self.data_path = data_path
        self.interrupt_handler_address = 0
        self.state = StateType.READY.value
        self.context = 0
        self.input_counter = 0

    def tick(self):
        self._tick += 1

    def latch_state(self, state: StateType):
        self.state = state.value

    def latch_context(self):
        self.context = self.prog_counter
        self.prog_counter = next((i for i, c in enumerate(self.code['code']) if c['opcode'] == 'inter_start'),
                                 self.context)

    def latch_prog_counter(self, sel: int):
        if sel == 1:
            self.prog_counter += 1
        elif sel == 2:
            instr = self.code['code'][self.prog_counter]
            arg = self.get_operand(instr)
            if arg is not None:
                self.prog_counter = arg
        elif sel == 3:
            self.prog_counter = self.context

    def get_operand(self, instr):
        if instr['operand']['value'] is None:
            return None
        op_type = instr['operand']['type']
        op_arg = instr['operand']['value']
        if op_type == OperandType.CONSTANT:
            self.data_path.put_data_reg(op_arg)
            return op_arg
        if op_type == OperandType.DIRECT:
            self.data_path.put_addr_reg(op_arg)
            self.data_path.latch_data_reg()
            self.tick()
            return self.data_path.data_register
        if op_type == OperandType.LABEL:
            self.data_path.put_addr_reg(op_arg)
            self.data_path.latch_data_reg()
            self.tick()
            return self.data_path.data_register
        if op_type == OperandType.INDIRECT:
            self.data_path.put_addr_reg(op_arg)
            self.data_path.latch_data_reg()
            self.tick()
            self.data_path.put_addr_reg(self.data_path.data_register)
            self.data_path.latch_data_reg()
            self.tick()
            return self.data_path.data_register
        else:
            return self.prog_counter

    def latch_interrupt_handler_address(self):
        for line in self.code:
            if line.get('opcode') == Opcode.INTER_START:
                self.interrupt_handler_address = line.get('operand').get('value')

    def get_curr_tick(self):
        return self._tick

    def interrupt_check(self):
        if self.state == StateType.INTERRUPTED.value or len(self.data_path.input_buffer) == 0:
            return
        if self.state == StateType.READY.value and self.get_curr_tick() >= self.data_path.input_buffer[0][0]:
            self.input_counter += 1
            self.latch_context()
            self.latch_state(StateType.INTERRUPTED)
            self.latch_prog_counter(2)
            logging.debug("Interrupted! TICK: {%d}", self.get_curr_tick())

    def decode_and_execute_instruction(self):
        self.interrupt_check()
        logging.debug('%s', self)
        instr = self.code['code'][self.prog_counter]
        opcode = instr['opcode']

        if opcode == Opcode.LD.value:
            self.get_operand(instr)
            self.data_path.latch_acc(SelectType.DATA)
            self.latch_prog_counter(1)
            self.tick()
        elif opcode == Opcode.ADD.value or opcode == Opcode.DIV.value or opcode == Opcode.MUL.value:
            self.get_operand(instr)
            self.data_path.write(opcode)
            self.tick()
            self.latch_prog_counter(1)
            self.tick()
        elif opcode == Opcode.INTER_START.value:
            self.latch_prog_counter(1)
            self.tick()
        elif opcode == Opcode.INTER_END.value:
            self.latch_state(StateType.READY)
            self.latch_prog_counter(3)
            self.tick()
            logging.debug('INTERRUPTION END')
        elif opcode == Opcode.ST.value:
            self.data_path.put_addr_reg(self.get_operand(instr))
            self.data_path.latch_memory()
            self.latch_prog_counter(1)
            self.tick()
        elif opcode == Opcode.JMP.value:
            self.latch_prog_counter(2)
            self.tick()
        elif opcode == Opcode.CMP.value:
            val = self.get_operand(instr)
            self.data_path.set_zero(self.data_path.acc - val)
            self.latch_prog_counter(1)
            self.tick()
        elif opcode == Opcode.JE.value:
            if self.data_path.zero():
                self.latch_prog_counter(2)
            else:
                self.latch_prog_counter(1)
        elif opcode == Opcode.OUT.value:
            self.data_path.output(True)
            self.tick()
            self.latch_prog_counter(1)
            self.tick()
        elif opcode == Opcode.IN.value:
            if self.data_path.has_next_token() == 0:
                raise EOFError
            self.data_path.acc = self.data_path.next_token()
            self.tick()
            self.latch_prog_counter(1)
            self.tick()
        elif opcode == Opcode.HLT.value:
            raise StopIteration
        else:
            logging.debug(opcode)
            assert False, 'Invalid Opcode!'

    def __repr__(self):
        state = f"{{TICK: {self._tick}, PC: {self.prog_counter}, ADDR: {self.data_path.address_register}, " \
                f"ACC: {self.data_path.acc}, DR: {self.data_path.data_register}}}"
        return state


def simulation(code, data_memory_size, limit, input_buffer):
    data_path = DataPath(data_memory_size, code['data'], input_buffer)
    control_unit = ControlUnit(code, data_path)
    instr_counter = 0

    try:
        while True:
            if limit <= instr_counter:
                logging.error("Limit is exceeded!")
                break
            control_unit.decode_and_execute_instruction()
            instr_counter += 1
    except StopIteration:
        pass

    logging.info('output_buffer: %s', repr(''.join(data_path.output_buffer)))
    logging.info('instructions : %s, ticks : %s', instr_counter, control_unit.get_curr_tick())
    return ''.join(data_path.output_buffer), instr_counter, control_unit.get_curr_tick()


def main2(args):
    arguments = parse_args()
    input_file = args[1]
    print('machine started')
    # machine_file = arguments.machine_file.read()
    program = json.loads(arguments.machine_file.read())
    print(program)

    if input_file != "":
        with open(input_file, encoding="utf-8") as file:
            input_text = file.read()
            if input_text == "":
                input_list = []
            else:
                input_list = literal_eval(input_text)
    else:
        input_list = []

    simulation(program, 32, 60, input_list)


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)
    main2(sys.argv[1:])

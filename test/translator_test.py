import pytest as pytest

from isa import OperandType, Opcode, Instruction, Operand
from translator import parse_text_block, parse_data_block, translate, OpcodeDto, OperandDto
from utils import prepare_code


@pytest.mark.parametrize("text_block, expected_text_mem, expected_text_mem_map", [
    ("ld val", [Instruction(Opcode.LD, Operand(OperandType.DIRECT, 'val'))], {}),
    ("jmp .exit", [Instruction(Opcode.JMP, Operand(OperandType.LABEL, '.exit'))], {}),
    ("ld #4", [Instruction(Opcode.LD, Operand(OperandType.CONSTANT, '4'))], {}),
    ("start:\nadd #4\nend:\nhlt", [Instruction(Opcode.ADD, Operand(OperandType.CONSTANT, '4')),
                                   Instruction(Opcode.HLT, None)], {'start': 0, 'end': 1}),
])
def test_parse_text_block(text_block, expected_text_mem, expected_text_mem_map):
    data_memory, data_memory_map = parse_text_block(text_block)
    assert data_memory == expected_text_mem
    assert data_memory_map == expected_text_mem_map


@pytest.mark.parametrize("data_block, expected_data_mem, expected_data_mem_map", [
    ("var: num 99", [99], {'var': 0}),
    ("a: num 1\nb: num 3", [1, 3], {'a': 0, 'b': 1}),
    ("str: str \"abc\"", [97, 98, 99], {'str': 0}),
])
def test_parse_data_block(data_block, expected_data_mem, expected_data_mem_map):
    data_memory, data_memory_map = parse_data_block(data_block)
    assert data_memory == expected_data_mem
    assert data_memory_map == expected_data_mem_map


@pytest.mark.parametrize("code, expected_data, expected_code", [
    (
            """
    section .data
        a: num 44
        b: num 555
        val: num 777
    section .text
            ld $val
            add #-1
        start:
            st val
            jmp .start
    """,
            [44, 555, 777],
            [OpcodeDto('ld', OperandDto('indirect_address', 2)),
             OpcodeDto('add', OperandDto('constant', -1)),
             OpcodeDto('st', OperandDto('direct_address', 2)),
             OpcodeDto('jmp', OperandDto('constant', 2))],
    ),
])
def test_parse_all(code, expected_data, expected_code):
    parsed_data, parsed_code = translate(prepare_code(code.split('\n')))
    assert parsed_code == expected_data
    assert parsed_data == expected_code

section .data
    curr_char: num 1
    hellow: str "hello world"
    null_term: num 0

section .text
    print:
        ld $curr_char
        cmp #0
        je .exit
        out
        ld curr_char
        add #1
        st #curr_char
        jmp .print
    exit:
        hlt

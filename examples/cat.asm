section .data
    null_term: num 0

section .text
	start:
        jmp .start
    interrupt:
        inter_start
        in
        out
        inter_end
	exit:
		hlt
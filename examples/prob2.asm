section .data
	first: num 1
	second: num 2
	current: num 0

section .text
	start:
		ld second
		add first
		out
		st #current
		ld second
		st #first
		ld current
		st #second
        jmp .start
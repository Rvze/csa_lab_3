# Лабораторная работа #3

Макаров Ньургун P33111

Вариант: `asm | acc | harv | hw | instr | struct | trap | port | prob2`

## Структура проекта

## Описание языка

Упрощённый язык ассемблера в расширенной форме Бэкуса — Наура:

```
<letter> ::= ["a"-"z"]
<name> ::= <letter> <name> | <letter>
<label> ::= "."<name> | <name>

<digit> ::= "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" 
<number> ::= <digit> <number> | <digit>

<empty_expr> ::= "\n"

<op_0_arg> ::= "hlt"
<op_1_arg> ::= "st" | "ld" | "add" | "mul" | "div" | "jmp" |
<label_def> ::= "."<name>":" | "section .text" | "section .data"

<line> ::= <label_def> | <op_0_arg> | <op_1_arg> " " <label> | <empty_expr>

<program> ::= <line> <program> | <line>
```

Система команд:

У операндов есть три режима адресации:

| имя        | описание                                                                                                   |
|:-----------|:-----------------------------------------------------------------------------------------------------------|
| `CONSTANT` | операнд хранится непосредственно в команде                                                                 |
| `DIRECT`   | операнд - это значение, лежащее по регистру                                                                |
| `INDIRECT` | операнд - это значение, лежащее по регистру, хранящемся в ячейке, на которую указывает команда (указатель) |

Ну, и метка конечно.

| мнемоника     | минимальное количество тактов | тип операнда              | описанаие команды                           |
|:--------------|-------------------------------|:--------------------------|:--------------------------------------------|
| `out`         | 2                             | `-`                       | распечатать в output stream значение из acc |
| `in`          | 2                             | `-`                       | прочитать в acc значение из input stream    |
| `hlt`         | -                             | `-`                       | поставить процессор на паузу                |
| `ld`          | 2                             | `const/register/indirect` | загрузить значение в acc                    |
| `st`          | 2                             | `register/indirect`       | сохранить значение из acc в память          |
| `add`         | 2                             | `const/register/indirect` | прибавить к acc аргумент                    |
| `mul`         | 2                             | `const/register/indirect` | умножить acc на значение                    |
| `div`         | 2                             | `const/register/indirect` | целочисленно поделить acc на значение       |
| `cmp`         | 1                             | `const/register/indirect` | сравнить acc со значением                   |
| `jmp`         | 1                             | `register`                | безусловный переход на аргумент-метку       |
| `je`          | 1                             | `register`                | переход на аргумент-метку, если равно       |
| `inter_start` | 1                             | `-`                       | начать прерывание                           |
| `inter_end`   | 1                             | `-`                       | завершить прерывание                        |

## Транслятор

Транслирует код на языке ассемблера в машинные инструкции (`.json`)

Пример исходного кода `cat.asm`:

```asm
section .text:
	start:
        jmp .start
    interrupt:
        inter_start
        in
        out
        inter_end
	exit:
		hlt
```

Результат трансляции `cat.out`:

```json
{
  "code": [
    {
      "opcode": "jmp",
      "operand": {
        "type": "label",
        "value": 0
      }
    },
    {
      "opcode": "inter_start",
      "operand": {
        "type": null,
        "value": null
      }
    },
    {
      "opcode": "in",
      "operand": {
        "type": null,
        "value": null
      }
    },
    {
      "opcode": "out",
      "operand": {
        "type": null,
        "value": null
      }
    },
    {
      "opcode": "inter_end",
      "operand": {
        "type": null,
        "value": null
      }
    },
    {
      "opcode": "hlt",
      "operand": {
        "type": null,
        "value": null
      }
    }
  ],
  "data": [
    0
  ]
}
```

# Модель процессора

## DataPath:

![img.png](img/datapath.png)

## ControlUnit:

![controlunit.png](img/controlunit.png)
Особенности процессора:

- Все операции построены вокруг аккумулятора `acc`, участвует в вводе-выводе
- Ввод-вывод осуществляется посимвольно через порты
- Машинное слово размером в 32 бит
- `program_counter` - счетчик инструкций, указывает на адрес исполняемой команды
- Реализована Гарвардская архитектура, две отдельные памяти для команд и для данных:
    - память команд:
        - `program_counter` - счетчик команд
    - память данных:
        - `addr_register` - для хранения адреса ячейки в памяти команд (по какому адресу читать/писать)
        - `data_register` - для хранения слова из памяти данных или записи в память данных

Журнал работы виртуальной машины для программы `hello.out`:

```
DEBUG:root:{TICK: 0, PC: 0, ADDR: 0, ACC: 0, DR: 0}
DEBUG:root:Interrupted! TICK: {1}
DEBUG:root:{TICK: 1, PC: 1, ADDR: 0, ACC: 0, DR: 0}
DEBUG:root:{TICK: 2, PC: 2, ADDR: 0, ACC: 0, DR: 0}
DEBUG:root:{TICK: 4, PC: 3, ADDR: 0, ACC: 104, DR: 0}
INFO:root:output: '' << 'h'
DEBUG:root:{TICK: 6, PC: 4, ADDR: 0, ACC: 104, DR: 0}
DEBUG:root:INTERRUPTION END
DEBUG:root:{TICK: 7, PC: 0, ADDR: 0, ACC: 104, DR: 0}
DEBUG:root:{TICK: 8, PC: 0, ADDR: 0, ACC: 104, DR: 0}
DEBUG:root:{TICK: 9, PC: 0, ADDR: 0, ACC: 104, DR: 0}
DEBUG:root:Interrupted! TICK: {10}
DEBUG:root:{TICK: 10, PC: 1, ADDR: 0, ACC: 104, DR: 0}
DEBUG:root:{TICK: 11, PC: 2, ADDR: 0, ACC: 104, DR: 0}
DEBUG:root:{TICK: 13, PC: 3, ADDR: 0, ACC: 101, DR: 0}
INFO:root:output: 'h' << 'e'
DEBUG:root:{TICK: 15, PC: 4, ADDR: 0, ACC: 101, DR: 0}
DEBUG:root:INTERRUPTION END
DEBUG:root:{TICK: 16, PC: 0, ADDR: 0, ACC: 101, DR: 0}
DEBUG:root:{TICK: 17, PC: 0, ADDR: 0, ACC: 101, DR: 0}
DEBUG:root:{TICK: 18, PC: 0, ADDR: 0, ACC: 101, DR: 0}
DEBUG:root:{TICK: 19, PC: 0, ADDR: 0, ACC: 101, DR: 0}
DEBUG:root:Interrupted! TICK: {20}
DEBUG:root:{TICK: 20, PC: 1, ADDR: 0, ACC: 101, DR: 0}
DEBUG:root:{TICK: 21, PC: 2, ADDR: 0, ACC: 101, DR: 0}
DEBUG:root:{TICK: 23, PC: 3, ADDR: 0, ACC: 108, DR: 0}
INFO:root:output: 'he' << 'l'
DEBUG:root:{TICK: 25, PC: 4, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:INTERRUPTION END
DEBUG:root:Interrupted! TICK: {26}
DEBUG:root:{TICK: 26, PC: 1, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 27, PC: 2, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 29, PC: 3, ADDR: 0, ACC: 108, DR: 0}
INFO:root:output: 'hel' << 'l'
DEBUG:root:{TICK: 31, PC: 4, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:INTERRUPTION END
DEBUG:root:{TICK: 32, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 33, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 34, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 35, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 36, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 37, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 38, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 39, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 40, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 41, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 42, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 43, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 44, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 45, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 46, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 47, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 48, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 49, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 50, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 51, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 52, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 53, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 54, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 55, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 56, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 57, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 58, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 59, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 60, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 61, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 62, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 63, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 64, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 65, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 66, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 67, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 68, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 69, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 70, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 71, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 72, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 73, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 74, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 75, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 76, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 77, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 78, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 79, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 80, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 81, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 82, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 83, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 84, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 85, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 86, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 87, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 88, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 89, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 90, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 91, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 92, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 93, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 94, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 95, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 96, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 97, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 98, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 99, PC: 0, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:Interrupted! TICK: {100}
DEBUG:root:{TICK: 100, PC: 1, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 101, PC: 2, ADDR: 0, ACC: 108, DR: 0}
DEBUG:root:{TICK: 103, PC: 3, ADDR: 0, ACC: 111, DR: 0}
INFO:root:output: 'hell' << 'o'
DEBUG:root:{TICK: 105, PC: 4, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:INTERRUPTION END
DEBUG:root:{TICK: 106, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 107, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 108, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 109, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 110, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 111, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 112, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 113, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 114, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 115, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 116, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 117, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 118, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 119, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 120, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 121, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 122, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 123, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 124, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 125, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 126, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 127, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 128, PC: 0, ADDR: 0, ACC: 111, DR: 0}
DEBUG:root:{TICK: 129, PC: 0, ADDR: 0, ACC: 111, DR: 0}
ERROR:root:Limit is exceeded!
INFO:root:output_buffer: 'hello'
INFO:root:instructions : 120, ticks : 130

```

## Апробация

Вариант: `asm | acc | harv | hw | instr | struct | trap | port | prob2`

| ФИО             | Алгоритм | LoC | code байт | code инстр | инстр. | такт |
|-----------------|----------|-----|-----------|------------|--------|------|
| Макаров Ньургун | hello    | 32  | -         | 9          | 120    | 130  |
| Макаров Ньургун | cat      | 17  | -         | 6          | 60     | 68   |
| Макаров Ньургун | prob2    | 31  | -         | 9          | 60     | 101  |

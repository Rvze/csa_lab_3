import json
from dataclasses import dataclass
from enum import Enum
from typing import Optional


class Opcode(str, Enum):
    LD = "ld"
    ST = "st"
    ADD = "add"
    MUL = "mul"
    DIV = "div"
    JMP = "jmp"
    HLT = "hlt"
    CMP = "cmp"
    JE = "je"
    OUT = "out"
    IN = "in"
    INTER_START = 'inter_start'
    INTER_END = 'inter_end'
    MOV = 'MOV'


class SelectType(int, Enum):
    INPUT = 0
    DATA = 1
    ALU = 2


class StateType(int, Enum):
    READY = 0
    INTERRUPTED = 1


class OperandType(str, Enum):
    DIRECT = "direct_address"
    INDIRECT = "indirect_address"
    CONSTANT = "constant"
    LABEL = "label"


@dataclass
class Operand:
    type: OperandType
    value: str


@dataclass
class Instruction:
    name: Opcode
    operand: Operand


args_count = {
    Opcode.LD.value: 1,
    Opcode.ST.value: 1,
    Opcode.ADD.value: 1,
    Opcode.MUL.value: 1,
    Opcode.DIV.value: 1,
    Opcode.JMP.value: 1,
    Opcode.HLT.value: 0,
    Opcode.CMP.value: 1,
    Opcode.JE.value: 1,
    Opcode.OUT.value: 0,
    Opcode.IN.value: 0,
    Opcode.INTER_START: 0,
    Opcode.INTER_END: 0
}


def write_code(filename, code, data):
    """Записать машинный код в файл."""
    with open(file=filename, mode='w', encoding='utf-8') as file:
        file.write(json.dumps(code, indent=4))

    with open(data, 'w', encoding='utf-8') as file:
        file.write(json.dumps(data, indent=4))


def write_data(filename, data):
    """Записать данные в файл."""
    with open(file=filename, mode='w', encoding='utf-8') as file:
        file.write(json.dumps(data, indent=4))


def read_code(filename):
    """Прочесть машинный код из файла"""
    with open(file=filename, encoding='utf-8') as file:
        code = json.loads(file.read())
    return code
